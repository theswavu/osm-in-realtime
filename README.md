# OSM In Realtime
Watch OSM get updated in real time!

**OSM In Realtime** is a simple visualization of the changes
made to [OpenStreetMap](https://openstreetmap.org) in (near) real-time.

## How It Works
OSM In Realtime downloads the minutely changeset feed provided by
OpenStreetMap and shows the changesets on the map.

## What It's For
This isn't really intended for any useful mapping purpose. I just
thought it would be a cool visualization.

## Created by [James Westman](https://www.jwestman.net/)
I make cool things. Sometimes they even work!

## Tech Stack
OSM In Realtime uses React and React-Leaflet. It's a static site hosted on
GitLab Pages.
