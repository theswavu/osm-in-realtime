# Third-Party Resources

## odometer.js, odometer-theme-minimal.css
License: MIT
URL: https://github.com/HubSpot/odometer

odometer-theme-minimal.css has been modified to change animation timing.

## Abel-Regular.woff2
License: Open Font License
URL: https://fonts.google.com/specimen/Abel
